package master;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;

import pages.agilepg;

import test1.base;
import test1.*;
public class regmaster {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("Masters clicked");
		test.createNode("click on Masters1");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addgroupmaser")
public Object[][] groupmaseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("groupm","excel/masters.xlsx");
	
  return object;	 
}



	@Test(dataProvider ="addgroupmaser", priority=2)
public void groupmaseraddf( String fg, String fdsc, String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.reggm.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("reg matster clicked then natureofbd ");
				oo.clickpg(o.regmaster);Thread.sleep(5000);
				
			}
		 	
			Thread.sleep(2000);
			oo.clickpg(o.reggm);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		
			 Thread.sleep(3000);
				oo.waits(o.bFORMATEGROUP);			
				
				Thread.sleep(3000);
				oo.textpg(o.FORMATEGROUP, fg);
				

				Thread.sleep(3000);
				oo.textpg(o.formatedescription, fdsc);
				
				Thread.sleep(4000);
				oo.clickpg(o.submit);
				 
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					 
					
					 Assert.assertEquals(o.uaalerts.getText(), "Electronic Signature Process Completed Successfully");	 
				
					 System.out.println("tt1");
					 oo.waits(o.bERES);	 
						Thread.sleep(4000);
					 oo.clickpg(o.ERES);
					 
					 System.out.println("tt2");
					 
					 
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("n role ");
					 ll.trace(o.uaalerts.getText());

					
					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);
			
			
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	
	





@DataProvider(name="addregassgn")
public Object[][] regassgnadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("addregassgn","excel/masters.xlsx");
	
  return object;	 
}



	@Test(dataProvider ="addregassgn", priority=3)
public void regassgnaddf( String blockname, String locationid,String areaname,String  equimentname,String department ,String searchcat,  String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.reggm.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("reg matster clicked then natureofbd ");
				oo.clickpg(o.regmaster);Thread.sleep(5000);
				
			}
		 
			Thread.sleep(2000);
			oo.clickpg(o.regasgn);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
		
			 Thread.sleep(3000);
				oo.waits(o.bblockname);			
				
				Thread.sleep(3000);
				oo.dropdownpg(o.blockname, blockname);
				Thread.sleep(3000);
				oo.dropdownpg(o.locationid, locationid);
				
				
				Thread.sleep(3000);
				oo.dropdownpg(o.areaname, areaname);
				
				Thread.sleep(3000);
				oo.dropdownpg(o.equimentname, equimentname);
				Thread.sleep(3000);
				oo.dropdownpg(o.department, department);
				
				oo.clickpg(o.regass);
				

				
				Thread.sleep(3000);
				oo.textpg(o.regsrch, searchcat);

				Thread.sleep(3000);
				oo.jsdownbye(o.submitreg);
				

				 List <WebElement> searchusize =driver.findElements(By.xpath(o.bregchbox1));
					
					System.out.println(searchusize.size()+"size......");
					
					
					
					if(searchusize.size()==1){	
			
				
				if(o.regchbox.isSelected()){
					System.out.println("already selected");
				}
				else{
				
				
				Thread.sleep(3000);
			oo.clickpg(o.regchbox);

			Thread.sleep(3000);
			

		oo.clickpg(o.submitreg);
			
			
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					 
					 
					
					 Assert.assertEquals(o.uaalerts.getText(), "Electronic Signature Process Completed Successfully");	 
				
					 System.out.println("tt1");
					 oo.waits(o.bERES);	 
						Thread.sleep(4000);
					 oo.clickpg(o.ERES);
					 
					 System.out.println("tt2");
					 
					 
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("n role ");
					 ll.trace(o.uaalerts.getText());

					
					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);
			
			
				}
				
				
				}else if(searchusize.size()==0){
					
					System.out.println("no search result fund");
				}else{
					System.out.println("result fund mutiple ");
					
				}
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	
	
	
	@Test( priority=6)
	 
	public void addclick() throws InterruptedException{
		
		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
