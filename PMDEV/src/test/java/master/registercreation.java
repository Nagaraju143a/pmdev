package master;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.xb.xsdschema.All;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;

import pages.agilepg;

import test1.base;

public class registercreation {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("plant clicked");
		test.createNode("click on plant2");
		 Assert.assertTrue(true);
}
 

///////////////////////////////////////////////////////////////////////



@DataProvider(name="addbmaster")
public Object[][] adminuseradddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("regcreate","excel/masters.xlsx");
	
  return object;	 
}


@Test(dataProvider ="addbmaster", priority=2)

public void adminuseraddf(String formategrp, String formatidd,String formatname,String frequency,  String vv) throws InterruptedException, IOException{



	 try {
		 System.out.println(" reg masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.regcreat.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("reg matster clicked then natureofbd ");
				oo.clickpg(o.regmaster);Thread.sleep(5000);
				
			}
		 
			Thread.sleep(2000);
			oo.clickpg(o.regcreat);
		
						 
		Thread.sleep(3000);
		oo.waits(o.baddbm);
		 
		 oo.clickpg(o.addbm);
	
		 
	
		 oo.waits(o.bformategrp);	 Thread.sleep(3000);	
	 oo.dropdownpg(o.formategrp, formategrp);	Thread.sleep(3000);
	 
	 oo.textpg(o.formatidd, formatidd);Thread.sleep(3000);	 
	 oo.textpg(o.formatname, formatname);Thread.sleep(3000);
	
	oo.dropdownpg(o.frequency, frequency);	Thread.sleep(3000);
	 
	oo.clickpg(o.reqf);Thread.sleep(3000);
		
	


	 //xpath based on text using
	 //System.out.println(driver.findElement(By.xpath("//label[text()='AREA ID']")).getText()+"txt1");	 
	 //below is checkbox values getting using label
	// System.out.println(driver.findElement(By.xpath("//label[text()='AREA ID']// ../input")).getAttribute("value")+"att");
	
	 
	 List <WebElement> ch =driver.findElements(o.bclreqid);
	 for(WebElement dk : ch){ 
		 double d = Double.parseDouble(vv);
			int i = (int) d;
					 System.out.println(i);
		 if( dk.getAttribute("value").equals(String.valueOf(i))){
				Thread.sleep(3000);
				
		
		 dk.click();
		 }
		 else if(vv.equals("all")){
			 
			 dk.click();
				 System.out.println(dk.getAttribute("value"));Thread.sleep(3000);
			 
		 }
	 }
		 
	 oo.clickpg(o.reqf);Thread.sleep(3000);
		 
			
		
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 driver.navigate().refresh();Thread.sleep(2000);
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);

	 }
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);

	
		}
	 
	 


@DataProvider(name="addrev")
public Object[][] addrev() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("regcreate1","excel/masters.xlsx");
	
  return object;	 
}





@Test(dataProvider ="addrev", priority=3)
public void rev1(String action, String title,String v1, String v2) throws InterruptedException, IOException{
		
		
		
		 try {
			 System.out.println(" reg masters  clicked");	
			 this.driver=base.driver;
				agilepg o = PageFactory.initElements(driver, agilepg.class);
				util oo = PageFactory.initElements(driver, util.class);
			 Thread.sleep(3000);
			 			 
			 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
			 
			 
			 
			 
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_END);
				robot.keyRelease(KeyEvent.VK_END);
				robot.keyRelease(KeyEvent.VK_CONTROL);
				
				
				
				
				Thread.sleep(5000);
				Actions abc= new Actions(driver);
				
				//WebElement we1=driver.findElement(By.xpath("//*[@id='builder-textarea']"));
				//System.out.println(we1.getText()+"hiiiiii");		
				//WebElement we2=driver.findElement(By.xpath("//*[@class='null col-xs-8 col-sm-9 col-md-10 formarea drag-container']"));
				
				
				if(action.equals("text")){
				
				//text
				Thread.sleep(4000);
				//abc.dragAndDrop(we1, we2).build().perform();
				abc.dragAndDrop(o.clctext, o.clcdiv).build().perform();
				
				Thread.sleep(2000);		      
			oo.clearpg(o.clcclr);	Thread.sleep(2000);
				oo.textpg(o.clcclr, title);
				Thread.sleep(2000);
				oo.clickpg(o.clcdat2);		
				Thread.sleep(2000);
				oo.textpg(o.clcdatav, v1);
				Thread.sleep(2000);
				oo.clickpg(o.clcdatas);
				
				}else if(action.equals("radio")){
				//radio button 
				
				
				//WebElement we3=driver.findElement(By.xpath("//*[@id='builder-radio']"));
				//Actions abcd= new Actions(driver);	
				Thread.sleep(4000);
				abc.dragAndDrop(o.clcradio, o.clcdiv).build().perform();
				 
				
				Thread.sleep(2000);		 
				oo.clearpg(o.clcclr);	Thread.sleep(2000);
				oo.textpg(o.clcclr, title);
				Thread.sleep(2000);
				
				
				oo.textpg(o.clcradio1, v1);		
				Thread.sleep(2000);	
			oo.clickpg(o.clcradio1c);
				Thread.sleep(2000);
			oo.textpg(o.clcradio2, v2);
				Thread.sleep(2000);	
				
				oo.clickpg(o.clcdatas);
				
				}
				
				else if(action.equals("select")){
				//select dropdown
				
				Thread.sleep(4000);
				abc.dragAndDrop(o.clcsel, o.clcdiv).build().perform();
				
				Thread.sleep(2000);		 
				oo.clearpg(o.clcclr);	Thread.sleep(2000);
				oo.textpg(o.clcclr, title);
				Thread.sleep(2000);
				
				oo.clickpg(o.clcdat2);			Thread.sleep(2000);
				
				oo.textpg(o.clcsel1, v1);		Thread.sleep(2000);
				oo.clickpg(o.clcselc);		Thread.sleep(2000);
				oo.textpg(o.clcsel2, v2);	
						
			Thread.sleep(2000);	
				
				oo.clickpg(o.clcdatas);
				}
				
				
				
			 
			 
		 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 driver.navigate().refresh();Thread.sleep(2000);
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
		//	
	
		}
	 








@DataProvider(name="addrev2")
public Object[][] addrev2() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("regcreate2","excel/masters.xlsx");
	
  return object;	 
}





@Test(dataProvider ="addrev2", priority=4)
public void rev2(String Remark, String p) throws InterruptedException, IOException{
		
		
		
		 try {
			 System.out.println(" reg masters  clicked");	
			 this.driver=base.driver;
				agilepg o = PageFactory.initElements(driver, agilepg.class);
				util oo = PageFactory.initElements(driver, util.class);
			 Thread.sleep(3000);
			 			 
			 driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
			 

			 Thread.sleep(3000);
				oo.jsdownbye(o.clwf1);Thread.sleep(3000);
				oo.clickpg(o.clwf1);Thread.sleep(2000);
				oo.clickpg(o.clwfi1);Thread.sleep(2000);
				
				
				oo.clickpg(o.clcprv);Thread.sleep(2000);
				oo.clickpg(o.clcsub1);
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, p);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					 
			
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
					 oo.waits(o.bERES);	 	Thread.sleep(2000);
					 oo.clickpg(o.ERES);	System.out.println("tested");
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("group master in checklist ");
					 ll.trace(o.uaalert.getText());
				
					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);


			 
		 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 




	 
	 
	@Test( priority=5)
	 
	public void addclick() throws InterruptedException{
		

		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);

		
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
