package master;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import adminsettingpg.util;

import pages.agilepg;

import test1.base;
import test1.*;
public class NatureBD {

public ExtentHtmlReporter htmlReporter;
public ExtentReports extent;
public ExtentTest test;
	
	public static WebDriver driver;
	
	@Test(priority = 1)
	public void adminc() throws InterruptedException {	System.out.println("testing...");
	 test = extent.createTest("Masters clicked");
		test.createNode("click on Masters1");
		 Assert.assertTrue(true);}
 
  
	






///////////////////////////////////////////////////////////////////////






@DataProvider(name="addbmaster")
public Object[][] naturebdadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("NatureBD","excel/masters.xlsx");
	
  return object;	 
}



	@Test(dataProvider ="addbmaster", priority=2)
public void naturebdaddf(String breakdownid, String Remark,String ppwd) throws InterruptedException, IOException{



	 try {
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.natureBD.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then natureofbd ");
				oo.clickpg(o.masters);Thread.sleep(5000);
				
			}
		 	
			Thread.sleep(2000);
			oo.clickpg(o.natureBD);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
			
			 Thread.sleep(3000);
				oo.waits(o.bbreakdownid);			
				
				Thread.sleep(3000);
				oo.textpg(o.breakdownid, breakdownid);
				
				
				
				Thread.sleep(4000);
				oo.clickpg(o.submit);
				 
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					 
					 
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
					 oo.waits(o.bERES);	 
					 oo.clickpg(o.ERES);
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("n role ");
					 ll.trace(o.uaalert.getText());


					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);
			
			
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	 
	
	


@DataProvider(name="bdinit")
public Object[][] bdinitadddata() throws IOException{
	 util oo = PageFactory.initElements(driver, util.class);
	Object[][] object=	oo.getDataFromDataprovider("bdinit","excel/masters.xlsx");
	
  return object;	 
}



	@Test(dataProvider ="bdinit", priority=3)
public void bdinitaddf(String AREAID,String EQUIPMENTID,String selectactivity,String PRODUCT,String BATCH,String NATUREOFBREAK, String Remark,String ppwd) throws InterruptedException, IOException{

		

	 try {
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.natureBD.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then bdinit ");
				oo.clickpg(o.masters);Thread.sleep(5000);
				
			}
		 	
			Thread.sleep(2000);
			oo.clickpg(o.BDinit);
			
			Thread.sleep(3000);
			oo.waits(o.baddbm);
			 
			 oo.clickpg(o.addbm);
			 
			 Thread.sleep(3000);
				oo.waits(o.bAREAID);	
			 
			 				
				Thread.sleep(3000);
				oo.dropdownpg(o.AREAID, AREAID);
				
				// application in prb drop down selecting
				Thread.sleep(3000);
				oo.dropdownpg(o.EQUIPMENTID, EQUIPMENTID);
				
				
				if(selectactivity.equalsIgnoreCase("YES")){
				Thread.sleep(3000);
				oo.dropdownpg(o.selectactivity, selectactivity);
				
				Thread.sleep(3000);
				oo.textpg(o.PRODUCT, PRODUCT);Thread.sleep(3000);
				oo.textpg(o.BATCH, BATCH);
				}else{
					
					Thread.sleep(3000);
					oo.dropdownpg(o.selectactivity, selectactivity);
				}
				
			
				
				Thread.sleep(3000);
				oo.dropdownpg(o.NATUREOFBREAK, NATUREOFBREAK);
				
		
				
				Thread.sleep(4000);
				oo.clickpg(o.submit);
				
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
					 oo.waits(o.bERES);	 
					 oo.clickpg(o.ERES);
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("break down init ");
					 ll.trace(o.uaalert.getText());


					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);
			
			
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	
	
	
	@DataProvider(name="bdusr")
	public Object[][] bdusradddata() throws IOException{
		 util oo = PageFactory.initElements(driver, util.class);
		Object[][] object=	oo.getDataFromDataprovider("bdusr","excel/masters.xlsx");
		
	  return object;	 
	}


	@Test(dataProvider ="bdusr", priority=4)
public void bdusraddf(String rolepvsearch,String roleuser,String user, String Remark,String ppwd) throws InterruptedException, IOException{

		

	 try {
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
		 Thread.sleep(3000);
		 
		 if(o.natureBD.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then bdinit ");
				oo.clickpg(o.masters);Thread.sleep(5000);
				
			}
		 	
			Thread.sleep(2000);
			oo.clickpg(o.BDusr);
			
			Thread.sleep(3000);
			oo.waits(o.brolepvsearch);
			 
			oo.textpg(o.rolepvsearch, rolepvsearch);
		
				Thread.sleep(3000);
			 
			 oo.clickpg(o.BDusrdiv1);
			
			 
			 
			 Thread.sleep(3000);
			 
			 oo.jsdownbye(o.roleuser);
					
			 
				
				 	
				Thread.sleep(3000);
				oo.dropdownpg(o.roleuser, roleuser);				
				Thread.sleep(3000);
				oo.dropdownpg(o.user, user);			
				
				Thread.sleep(4000);
				oo.clickpg(o.reinitiate);
				
				
				 Thread.sleep(3000);
					oo.waits(o.bRemark);
					
					Thread.sleep(2000);
					 oo.textpg(o.Remark, Remark);
					Thread.sleep(2000);
					 oo.textpg(o.pwd, ppwd);
					 oo.clickpg(o.validateuser);	Thread.sleep(4000);
					
					 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
					 oo.waits(o.bERES);	 
					 oo.clickpg(o.ERES);
					 oo.waits(o.bokbtn);Thread.sleep(4000); 
					 Logger ll =Logger.getLogger("break down init ");
					 ll.trace(o.uaalert.getText());


					 oo.clickpg(o.okbtn);
					 Thread.sleep(4000);
			
			
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 //
	

	@DataProvider(name="bdclsr")
	public Object[][] bdclsradddata() throws IOException{
		 util oo = PageFactory.initElements(driver, util.class);
		Object[][] object=	oo.getDataFromDataprovider("bdclsr","excel/masters.xlsx");
		
	  return object;	 
	}


	@Test(dataProvider ="bdclsr", priority=5)
public void bdclsraddf(String n, String p, String rolepvsearch, String CHANGECONTROLINITIATED, String CHANGECONTROLNUMBER,String VALIDATIONCOMMENTS, String VALIDATIONCONCLUSION, String correctiveaction) throws InterruptedException, IOException{

		

	 try {
		 
		 
		 
		 
		 System.out.println(" masters  clicked");	
		 this.driver=base.driver;
			agilepg o = PageFactory.initElements(driver, agilepg.class);
			util oo = PageFactory.initElements(driver, util.class);
			
////	
			
			 Properties u= new Properties();	u.load(ds.class.getResourceAsStream("object.properties"));
			 
		
		
		
		
			
			Thread.sleep(4000);
			oo.waits(o.blogout1);
			oo.clickpg(o.logout1);
		Thread.sleep(3000);//oo.clickpg(o.usrreg);
		oo.clickpg(o.logout2);
		Thread.sleep(3000);
		driver.get(u.getProperty("url"));
		Thread.sleep(4000);
		if(n.contains(".")){
			oo.textipg(o.loginname, n);
		}else{
		oo.textpg(o.loginname, n);
		
		}
		
		
		
				
	Thread.sleep(5000);
	oo.textpg(o.loginpassword, p);

	Thread.sleep(4000);
	oo.clickpg(o.loginsub);

	Thread.sleep(3000);
	
		 
		 if(o.BDclsr.isDisplayed()){	 Thread.sleep(3000);
			
			System.out.println("clicked ");}
		 else{
			 System.out.println("matster clicked then bdinit ");
				oo.clickpg(o.masters);Thread.sleep(5000);
				
			}
		 	
			Thread.sleep(2000);
			oo.clickpg(o.BDclsr);
			
			Thread.sleep(3000);
			oo.waits(o.brolepvsearch);
			 
			oo.textpg(o.rolepvsearch, rolepvsearch);
		
				Thread.sleep(3000);
			 
			 oo.clickpg(o.BDusrdiv1);
			 Thread.sleep(2000);
			
			oo.jsclickpg(o.CHANGECONTROLINITIATED);
			 Thread.sleep(2000);
	
			oo.dropdownpg(o.CHANGECONTROLINITIATED, CHANGECONTROLINITIATED);
		
			Thread.sleep(2000);			
			oo.textpg(o.CHANGECONTROLNUMBER, CHANGECONTROLNUMBER);
			Thread.sleep(2000);			
			oo.textpg(o.VALIDATIONCOMMENTS, VALIDATIONCOMMENTS);
			Thread.sleep(2000);			
			oo.textpg(o.VALIDATIONCONCLUSION, VALIDATIONCONCLUSION);
			Thread.sleep(2000);			
			oo.textpg(o.correctiveaction, correctiveaction);
			Thread.sleep(4000);	
			oo.clickpg(o.reinitiate);
			
			///
			
			 Thread.sleep(3000);
				oo.waits(o.bRemark);
				
				Thread.sleep(2000);
				 oo.textpg(o.Remark, "ok");
				Thread.sleep(2000);
				 oo.textpg(o.pwd, p);
				 oo.clickpg(o.validateuser);	Thread.sleep(4000);
				
				 Assert.assertEquals(o.uaalert.getText(), "Electronic Signature Process Completed Successfully");	 
				 oo.waits(o.bERES);	 
				 oo.clickpg(o.ERES);
				 oo.waits(o.bokbtn);Thread.sleep(4000); 
				 Logger ll =Logger.getLogger("break down init ");
				 ll.trace(o.uaalert.getText());


				 oo.clickpg(o.okbtn);
				 Thread.sleep(4000);
			
			
			
			
				
		 
	 }catch (Exception e)
	 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
	// TODO: handle exception
	 Logger ll =Logger.getLogger("n role ");
	 ll.trace(e.getMessage());
	 Thread.sleep(4000);
		util oo = PageFactory.initElements(driver, util.class);
	oo.srnsht(driver);
	 	}
	 	test = extent.createTest("adminuseraddf");
		test.createNode("useradd with valid input");
		 Assert.assertTrue(true);
		 Thread.sleep(2000);
			driver.navigate().refresh();Thread.sleep(2000);
	
		}
	 
	
	
	 
	 
	
	 
	
	
	
	@Test( priority=6)
	 
	public void addclick() throws InterruptedException{
		
		System.out.println("testing...");
		 test = extent.createTest("plant clicked");
			test.createNode("click on plant2");
			 Assert.assertTrue(true);
		
	}
	 
	 
	 
	 



@BeforeTest
public void setExtent() {
	

 
	String dateName = new SimpleDateFormat("ddMMyy HHmmss").format(new Date());
 htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/"+dateName+"myReport.html");

 htmlReporter.config().setDocumentTitle("Automation Report"); 
 htmlReporter.config().setReportName("Functional Testing"); 
 htmlReporter.config().setTheme(Theme.DARK);
 
 extent = new ExtentReports();
 extent.attachReporter(htmlReporter);
 
 
 extent.setSystemInfo("Host name", "localhost");
 extent.setSystemInfo("Environemnt", "QA");
 extent.setSystemInfo("user", "naga");
}

@AfterTest
public void endReport() {
 extent.flush();
}






 
@AfterMethod
public void tearDown(ITestResult result) throws IOException {
	
	System.out.println(result.getStatus()+"dddddddddd");
 if (result.getStatus() == ITestResult.FAILURE) {
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); 
  test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); 
  
  String screenshotPath = util.getScreenshot(driver, result.getName());
  
  test.addScreenCaptureFromPath(screenshotPath);
 }
 else if (result.getStatus() == ITestResult.SUCCESS) {
	  test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
}
 else if (result.getStatus() == ITestResult.SKIP) {
  test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
 }

	
}



}
