
package pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



import org.openqa.selenium.Keys;

public class agilepg {
	
	WebDriver driver;
/*
	@FindBy(xpath="//input[@value='Login']")  public WebElement aaaaa;
	public By aaaaa =By.xpath("//input[@value='Login']");
	
	extending one class into other class is caled inheritance 
		
	*/
	
	

	
	//login
	@FindBy(xpath="//*[@id='username']")  public WebElement loginname;
	public By bloginname =By.xpath("//*[@id='username']");
	
	@FindBy(xpath="//*[@id='password']")  public WebElement loginpassword;
	public By bloginpassword =By.xpath("//*[@id='password']");
	
	/*@FindBy(xpath="//*[@value='LogIn']")  public WebElement loginsub;
	public By bloginsub =By.xpath("//*[@value='LogIn']");*/
	
	
	@FindBy(xpath="/html/body/div/div/div/div[2]/form/div[6]/input")  public WebElement loginsub;
	public By bloginsub =By.xpath("/html/body/div/div/div/div[2]/form/div[6]/input");
	
	
	
	
	

	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[2]/a/i")  public WebElement masers;
	
	public By bmasers = By.xpath("//*[@id='sidebar-menu']/div/ul/li[2]/a/i");
	
	@FindBy(xpath="//*[@id='sidebar-menu']/div/ul/li[1]/a/i")  public WebElement administrator;
	
	public By badministrator = By.xpath("//*[@id='sidebar-menu']/div/ul/li[1]/a/i");
	
	
	//a[contains(@href,'RegistereduserList')]// ..// ..// ..//a/i
	@FindBy(xpath="//a[contains(@href,'BlockMasterList')]// ..// ..// ..//a/i")  public WebElement masters;
	public By bmasters = By.xpath("//a[contains(@href,'BlockMasterList')]// ..// ..// ..//a/i");

	

	
	//natureBD

	@FindBy(xpath="//a[contains(@href,'BreakdownList')]")  public WebElement natureBD;
	public By bnatureBD = By.xpath("//a[contains(@href,'BreakdownList')]");
	
	@FindBy(xpath="//*[@class='collapse-link']")  public WebElement addbm;
	public By baddbm = By.xpath("//*[@class='collapse-link']");
	
	@FindBy(xpath="//*[@id='breakdownid']")  public WebElement breakdownid;
	public By bbreakdownid =By.xpath("//*[@id='breakdownid']");
	
	
	
	@FindBy(xpath="//*[@id='btnsubmit']")  public WebElement submit;
	public By bsubmit =By.xpath("//*[@id='btnsubmit']");
	 
	
	//nature of break down search
	@FindBy(xpath="//*[@id='datatable-responsive_filter']/label/input")  public WebElement fsearch1;
	
	public By bfsearch1 =By.xpath("//*[@id='datatable-responsive_filter']/label/input");
	
	// electronic sign
			@FindBy(xpath="//*[@name='Remark']")  public WebElement Remark;
			public By bRemark = By.xpath("//*[@name='Remark']");	
			@FindBy(xpath="//*[@name='pwd']")  public WebElement pwd;
			public By bpwd = By.xpath("//*[@name='pwd']");	
			@FindBy(xpath="//*[@name='validateuser']")  public WebElement validateuser;
			public By bvalidateuser = By.xpath("//*[@name='validateuser']");
			//*[@id="myModal1"]/div/div/div[1]/div/div[1]/h4
			@FindBy(xpath="//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4")  public WebElement uaalert;
			public By buaalert = By.xpath("//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4");
			public String buaalert1 ="//*[@id='myModal1']/div/div/div[1]/div/div[1]/h4";
			@FindBy(xpath="//*[@id='ERES']")  public WebElement ERES;
			public By bERES = By.xpath("//*[@id='ERES']");
			@FindBy(xpath="//*[@id='okbtn']")  public WebElement okbtn;
			public By bokbtn = By.xpath("//*[@id='okbtn']");
			@FindBy(xpath="//*[@id='okbtn1']")  public WebElement okbtn1;
			public By bokbtn1 = By.xpath("//*[@id='okbtn1']");
			
			@FindBy(xpath="//*[@id='Errorbtn']")  public WebElement Errorbtn;
			public By bErrorbtn = By.xpath("//*[@id='Errorbtn']");
	//break down initiator
			
			
			@FindBy(xpath="//a[contains(@href,'breakdowninitiationlist')]")  public WebElement BDinit;
			public By bBDinit = By.xpath("//a[contains(@href,'breakdowninitiationlist')]");
			
			@FindBy(xpath="//*[@id='AREAID']")  public WebElement AREAID;
			public By bAREAID = By.xpath("//*[@id='AREAID']");
			
			@FindBy(xpath="//*[@id='EQUIPMENTID']")  public WebElement EQUIPMENTID;
			public By bEQUIPMENTID = By.xpath("//*[@id='EQUIPMENTID']");
						
			@FindBy(xpath="//*[@id='selectactivity']")  public WebElement selectactivity;
			public By bselectactivity = By.xpath("//*[@id='selectactivity']");
			@FindBy(xpath="//*[@id='PRODUCT']")  public WebElement PRODUCT;
			public By bPRODUCT = By.xpath("//*[@id='PRODUCT']");
			@FindBy(xpath="//*[@id='BATCH']")  public WebElement BATCH;
			public By bBATCH = By.xpath("//*[@id='BATCH']");
			
			@FindBy(xpath="//*[@id='NATUREOFBREAK']")  public WebElement NATUREOFBREAK;
			public By bNATUREOFBREAK = By.xpath("//*[@id='NATUREOFBREAK']");
			
			
			
//Break Down Assign to User
			@FindBy(xpath="//a[contains(@href,'breakassigntouserlist')]")  public WebElement BDusr;
			public By bBDusr = By.xpath("//a[contains(@href,'breakassigntouserlist')]");

			@FindBy(xpath="//*[@id='datatable-responsive_filter']/label/input")  public WebElement rolepvsearch;
			public By brolepvsearch =By.xpath("//*[@id='datatable-responsive_filter']/label/input");
			//3333 //*[@id="datatable-responsive"]/tbody/tr/td[1]/a/u
			
			@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u")  public WebElement BDusrdiv1;
			public By bBDusrdiv1 =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u");
			
			@FindBy(xpath="//*[@id='roleuser']")  public WebElement roleuser;
			public By broleuser = By.xpath("//*[@id='roleuser']");
			@FindBy(xpath="//*[@id='user']")  public WebElement user;
			public By buser = By.xpath("//*[@id='user']");
			
			@FindBy(xpath="//*[@id='reinitiate']")  public WebElement reinitiate;
			public By breinitiate = By.xpath("//*[@id='reinitiate']");
			
		
			
//break Down closer
			@FindBy(xpath="//a[contains(@href,'breakdowncloselsit')]")  public WebElement BDclsr;
			public By bBDclsr = By.xpath("//a[contains(@href,'breakdowncloselsit')]");
			
			
	
			@FindBy(xpath="//*[@id='breakdowncloselsit']")  public WebElement breakdowncloselsit;
			public By bbreakdowncloselsit = By.xpath("//*[@id='breakdowncloselsit']");
			
			
			@FindBy(xpath="//*[@id='CHANGECONTROLINITIATED']")  public WebElement CHANGECONTROLINITIATED;
			public By bCHANGECONTROLINITIATED = By.xpath("//*[@id='CHANGECONTROLINITIATED']");
			
			@FindBy(xpath="//*[@id='CHANGECONTROLNUMBER']")  public WebElement CHANGECONTROLNUMBER;
			public By bCHANGECONTROLNUMBER = By.xpath("//*[@id='CHANGECONTROLNUMBER']");
			
			@FindBy(xpath="//*[@id='VALIDATIONCOMMENTS']")  public WebElement VALIDATIONCOMMENTS;
			public By bVALIDATIONCOMMENTS = By.xpath("//*[@id='VALIDATIONCOMMENTS']");
			@FindBy(xpath="//*[@id='VALIDATIONCONCLUSION']")  public WebElement VALIDATIONCONCLUSION;
			public By bVALIDATIONCONCLUSION = By.xpath("//*[@id='VALIDATIONCONCLUSION']");
			@FindBy(xpath="//*[@id='correctiveaction']")  public WebElement correctiveaction;
			public By bcorrectiveaction = By.xpath("//*[@id='correctiveaction']");
			
			
			
			
		
			
			@FindBy(xpath="//*[@class='user-profile dropdown-toggle']")  public WebElement logout1;
			public By blogout1 = By.xpath("//*[@class='user-profile dropdown-toggle']");

			/*@FindBy(xpath="//a[@href='/PMTSBV/UI/Login/Logout']") public WebElement logout2;*/
			@FindBy(xpath="//a[contains(@href,'Login/Logout1')]")  public WebElement logout2;
			
			
			
//Register Master
			@FindBy(xpath="//a[contains(@href,'FormateGroupMasterList')]// ..// ..// ..//a/i")  public WebElement regmaster;
			public By bregmaster = By.xpath("//a[contains(@href,'FormateGroupMasterList')]// ..// ..// ..//a/i");
			
	//Group Master		
			@FindBy(xpath="//a[contains(@href,'FormateGroupMasterList')]")  public WebElement reggm;
			public By breggm = By.xpath("//a[contains(@href,'FormateGroupMasterList')]");	
			
			
			@FindBy(xpath="//*[@id='FORMATEGROUP']")  public WebElement FORMATEGROUP;
			public By bFORMATEGROUP = By.xpath("//*[@id='FORMATEGROUP']");
			@FindBy(xpath="//*[@id='formatedescription']")  public WebElement formatedescription;
			public By bformatedescription = By.xpath("//*[@id='formatedescription']");
			//*[@id="myModal1"]/div/div/div[1]/div/div[1]/h4
			@FindBy(xpath="//*[@id='myModal']/div/div/div[1]/div/div[1]/h4")  public WebElement uaalerts;
			public By buaalerts = By.xpath("//*[@id='myModal']/div/div/div[1]/div/div[1]/h4");
			///btnsubmit
			
//Register assigning
			
	
			@FindBy(xpath="//a[contains(@href,'FormatAssigningDetails')]")  public WebElement regasgn;
			public By bregasgn = By.xpath("//a[contains(@href,'FormatAssigningDetails')]");	
			@FindBy(xpath="//*[@id='blockname']")  public WebElement blockname;
			public By bblockname = By.xpath("//*[@id='blockname']");
			@FindBy(xpath="//*[@id='locationid']")  public WebElement locationid;
			public By blocationid = By.xpath("//*[@id='locationid']");
			
			@FindBy(xpath="//*[@id='areaname']")  public WebElement areaname;
			public By bareaname = By.xpath("//*[@id='areaname']");
			@FindBy(xpath="//*[@id='equimentname']")  public WebElement equimentname;
			public By bequimentname = By.xpath("//*[@id='equimentname']");
			@FindBy(xpath="//*[@id='department']")  public WebElement department;
			public By bdepartment = By.xpath("//*[@id='department']");
			
			@FindBy(xpath="//*[@id='regform']/div[1]/div[1]/div[8]/div/button[1]")  public WebElement regass;
			public By bregass = By.xpath("//*[@id='regform']/div[1]/div[1]/div[8]/div/button[1]");
			
			//*[@id="CityInfo_filter"]/label/input
			@FindBy(xpath="//*[@id='CityInfo_filter']/label/input")  public WebElement regsrch;
			public By bregsrch = By.xpath("//*[@id='CityInfo_filter']/label/input");
			
			
			//*[@id="cbs_10"]
			@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]")  public WebElement regchbox;
			public By bregchbox = By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]");
			public String bregchbox1 ="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[1]/div[2]/div/div/div[2]/div/table/tbody/tr/td[4]/input[1]";
			
			@FindBy(xpath="//*[@id='submit']")  public WebElement submitreg;
			public By bsubmitreg = By.xpath("//*[@id='submit']");
			
//register creation

			@FindBy(xpath="//a[contains(@href,'FrontendFormDisplayList')]")  public WebElement regcreat;
			public By bregcreat = By.xpath("//a[contains(@href,'FrontendFormDisplayList')]");	
			
			@FindBy(xpath="//*[@id='formategrp']")  public WebElement formategrp;
			public By bformategrp = By.xpath("//*[@id='formategrp']");
			
			@FindBy(xpath="//*[@id='formatidd']")  public WebElement formatidd;
			public By bformatidd = By.xpath("//*[@id='formatidd']");
			@FindBy(xpath="//*[@id='formatname']")  public WebElement formatname;
			public By bformatname = By.xpath("//*[@id='formatname']");
			@FindBy(xpath="//*[@id='frequency']")  public WebElement frequency;
			public By bfrequency = By.xpath("//*[@id='frequency']");
			
			@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/div[7]/div[2]/button")  public WebElement reqf;
			public By breqf = By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/form/div[2]/div[7]/div[2]/button");
			@FindBy(xpath="//*[@id='ingredientcode1']")  public WebElement clreqid;
			public By bclreqid =By.xpath("//*[@id='ingredientcode1']");
			
			

			@FindBy(xpath="//*[@id='builder-textarea']")  public WebElement clctext;
			public By bclctext =By.xpath("//*[@id='builder-textarea']");
			@FindBy(xpath="//*[@class='null col-xs-8 col-sm-9 col-md-10 formarea drag-container']")  public WebElement clcdiv;
			public By bclcdiv =By.xpath("//*[@class='null col-xs-8 col-sm-9 col-md-10 formarea drag-container']");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/input")  public WebElement clcclr;
			public By bclcclr =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/input");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/ul/li[2]/a")  public WebElement clcdat2;
			public By bclcdat2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/ul/li[2]/a");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div/textarea")  public WebElement clcdatav;
			public By bclcdatav =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div/textarea");
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/button[1]")  public WebElement clcdatas;
			public By bclcdatas =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/button[1]");
			
			
			
			@FindBy(xpath="//*[@id='builder-radio']")  public WebElement clcradio;
			public By bclcradio =By.xpath("//*[@id='builder-radio']");
			
			
			

			               
			               
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tbody/tr/td[1]/div/input")  public WebElement clcradio1;
			public By bclcradio1 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tbody/tr/td[1]/div/input");
			

			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tfoot/tr/td/button")  public WebElement clcradio1c;
			public By bclcradio1c =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tfoot/tr/td/button");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tbody/tr[2]/td[1]/div/input")  public WebElement clcradio2;
			public By bclcradio2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[1]/div[7]/table/tbody/tr[2]/td[1]/div/input");
			

			
			
			@FindBy(xpath="//*[@id='builder-select']")  public WebElement clcsel;
			public By bclcsel =By.xpath("//*[@id='builder-select']");
			
				
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr/td[1]/div/input")  public WebElement clcsel1;
			public By bclcsel1 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr/td[1]/div/input");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tfoot/tr/td/button")  public WebElement clcselc;
			public By bclcselc =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tfoot/tr/td/button");
			
			
			@FindBy(xpath="/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr[2]/td[1]/div/input")  public WebElement clcsel2;
			public By bclcsel2 =By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]/div[4]/table/tbody/tr[2]/td[1]/div/input");
			

			@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[6]/div[1]/button")  public WebElement clwf1;
			public By bclwf1 =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[6]/div[1]/button");
			
			

			@FindBy(xpath="//*[@value='Supervisor'and @id='intiator']")  public WebElement clwfi1;
			public By bclwfi1 =By.xpath("//*[@value='Supervisor'and @id='intiator']");
			
	           @FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[9]/div/button[1]")  public WebElement clcprv;
		       public By bclcprv =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[2]/div[9]/div/button[1]");
				@FindBy(xpath="//*[@id='submit1']")  public WebElement clcsub1;
				public By bclcsub1 =By.xpath("//*[@id='submit1']");
				
				
 //De-active Nature of Break down
				
				@FindBy(xpath="//a[contains(@href,'#menu11')]")  public WebElement drs;
				public By bdrs =By.xpath("//a[contains(@href,'#menu11')]");
			
				//*[@id="datatable-responsive_filter']/label/input
				@FindBy(xpath="//*[@id='datatable-responsives_filter']/label/input")  public WebElement fsearch;				
				public By bfsearch =By.xpath("//*[@id='datatable-responsives_filter']/label/input");
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr")  public WebElement rowsnbd;
				public By browsnbd =By.xpath("//*[@id='datatable-responsives']/tbody/tr");		
				public String browsnbd1 ="//*[@id='datatable-responsives']/tbody/tr";
				public String browsnbd11 ="//*[@id='datatable-responsive']/tbody/tr";
				public String bdivnbd11 ="//*[@id='datatable-responsives']/tbody/tr/td";
				public String bdivnbd11a ="//*[@id='datatable-responsive']/tbody/tr/td";
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]")  public WebElement divnbd;
				public By bdivnbd =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]");		
				public String bdivnbd1 ="//*[@id='datatable-responsives']/tbody/tr/td[4]";
			
		
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[3]/i")  public WebElement nbdprev;
				public By bnbdprev =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[3]/i");
				
				
				@FindBy(xpath="//*[@id='Deactive']")  public WebElement deactive;
				public By bdeactive =By.xpath("//*[@id='Deactive']");
				
//deactive nature breakdown intit  //*[@id="datatable-responsives']/tbody/tr/td[4]
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[5]")  public WebElement divnbdi;
				
				
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]/i")  public WebElement nbdprevi;
				public By bnbdprevi =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]/i");
				
				
	//Deactive Registration Group Master 			

				@FindBy(xpath="//*[@id='regform']/div[3]/button[1]")  public WebElement drgm;
				public By bdrgm =By.xpath("//*[@id='regform']/div[3]/button[1]");


	// Deactive register creation 			
				
				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[5]")  public WebElement divrc;
				public By bdivrc=By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[5]");		
				public String bdivrc1 ="//*[@id='datatable-responsives']/tbody/tr/td[5]";

				@FindBy(xpath="//*[@id='datatable-responsives']/tbody/tr/td[4]/i")  public WebElement revrc;
				public By brevrc =By.xpath("//*[@id='datatable-responsives']/tbody/tr/td[4]/i");
				
				
				@FindBy(xpath="/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[11]/button[1]")  public WebElement subrc;
				public By bsubrc =By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div/div[2]/div[11]/button[1]");
				
				//*[@id="datatable-responsive"]/tbody/tr/td[1]/a/u
				
				
				@FindBy(xpath="//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u")  public WebElement raclick;
				public By braclick =By.xpath("//*[@id='datatable-responsive']/tbody/tr/td[1]/a/u");
				
				@FindBy(xpath="//*[@id='drop']")  public WebElement drop;
				public By bdrop =By.xpath("//*[@id='drop']");
				
				
				//agile
						
						
						
						
						
	public agilepg(WebDriver driver){ 
        this.driver=driver; 
        
        
        // driver from tc assigned to  webdriver  driver by using this.driver here assigned to our WebDriver driver;
}

	
	public void waits(By  userName2){
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated((By) userName2));
		System.out.println(" wait for ............");
		
	}
	
	

	public void clearpg(WebElement  name2)
	{
		
		
		name2.clear();
	}
	
	
	
	
	
	
	
	
	
	
	

	}
